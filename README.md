Rack Shelf
==========

Easily integrate a Rack application into AWS Lambda.

Provides adapters to convert AWS Lambda events to Rack environment requests
and Rack responses to AWS Lambda proxy integrations.

Shelf currently works with API Gateway.

Installation
------------

Add to your Gemfile:

```ruby
gem 'rack-shelf'
```

or gemspec:

```ruby
spec.add_dependency 'rack-shelf'
```

Usage
-----

In your AWS Lambda handler file:

```ruby
require 'rack/shelf'

class App
  def call
    [200, {}, ['Hello world!']]
  end
end

def handler(event:, context:)
  Rack::Shelf.api_gateway(App.new, event, context)
end
```

### Detailed Explanation

The `#api_gateway` method indicates the Lambda is expected to be invoked by API Gateway.
The method accepts three arguments: the Rack application, the Lambda event, and the Lambda context.
Shelf will translate the Lambda information to a Rack environment instance and invoke `#call` on the application.
It then takes the return value of `#call` (the three-element array) and translates it to a hash.
That hash contains the expected keys and values for an AWS Lambda proxy integration.

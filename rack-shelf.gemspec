# frozen_string_literal: true

$LOAD_PATH.unshift File.expand_path('lib', __dir__)
require 'rack/shelf/version'

Gem::Specification.new do |s|
  s.name          = 'rack-shelf'
  s.version       = Rack::Shelf::VERSION
  s.authors       = ['Michael Miller']
  s.email         = ['icy.arctic.fox@gmail.com']
  s.homepage      = 'https://gitlab.com/arctic-fox/rack-shelf'
  s.licenses      = ['MIT']
  s.summary       = 'Adapts AWS Lambda event sources to Rack environments.'
  s.description   = 'Provides a translation for AWS Lambda events ' \
                    'to Rack environments.'

  s.files         = Dir.glob('{bin/*,lib/**/*,[A-Z]*}')
  s.platform      = Gem::Platform::RUBY
  s.require_paths = ['lib']

  s.add_dependency 'rack'

  s.add_development_dependency 'rubocop', '~> 0.80.1'
end

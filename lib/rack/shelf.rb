# frozen_string_literal: true

require_relative 'shelf/api_gateway'
require_relative 'shelf/base64_response_adapter'
require_relative 'shelf/environment_builder'
require_relative 'shelf/response_adapter'
require_relative 'shelf/version'

module Rack
  # Adapts AWS Lambda event sources to Rack environments.
  module Shelf
    extend self

    # Runs a Rack application, translating the request and response.
    # This method assumes the Lambda event came from API Gateway.
    # @param app [#call] Rack application to call.
    # @param event [Hash] Lambda event hash.
    # @param context [Object] Lambda context object.
    # @return [Hash] AWS Lambda response.
    def api_gateway(app, event, context)
      run(APIGateway, ResponseAdapter, app, event, context)
    end

    # Runs a Rack application, translating the request and response.
    # This method assumes the Lambda event came from API Gateway.
    # The response body is encoded as base-64.
    # @param app [#call] Rack application to call.
    # @param event [Hash] Lambda event hash.
    # @param context [Object] Lambda context object.
    # @return [Hash] AWS Lambda response.
    def api_gateway_base64(app, event, context)
      run(APIGateway, Base64ResponseAdapter, app, event, context)
    end

    private

    # Runs the app and translates the request and response.
    # @param request_adapter [#env] Translates the Lambda event.
    # @param response_adapter [#convert, #error] Translates the Rack response.
    # @param app [#call] Rack application to call.
    # @param event [Hash] Lambda event hash.
    # @param context [Object] Lambda context object.
    # @return [Hash] AWS Lambda response.
    def run(request_adapter, response_adapter, app, event, context)
      env      = request_adapter.env(event, context)
      response = app.call(env)
      response_adapter.convert(response)
    rescue StandardError => e
      response_adapter.error(e)
    end
  end
end

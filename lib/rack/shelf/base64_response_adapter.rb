# frozen_string_literal: true

require 'base64'
require_relative 'response_adapter'

module Rack
  module Shelf
    # Transforms a standard Rack response array
    # to a return value required by AWS Lambda.
    # Encodes the response body as base-64.
    # This is typically used for sending binary data (not text).
    class Base64ResponseAdapter < ResponseAdapter
      # Constructs the AWS Lambda response.
      # @return [Hash]
      def build
        super.merge('isBase64Encoded' => true)
      end

      private

      # Constructs the response body encoded in base-64.
      # @return [String]
      def body
        Base64.encode64(super)
      end
    end
  end
end

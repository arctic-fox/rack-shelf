# frozen_string_literal: true

module Rack
  module Shelf
    class APIGateway
      # Extracts API Gateway information in version 2.0 format.
      class V2Payload
        # Produces a Rack compatible environment instance
        # from a Lambda event and context.
        # @param builder [EnvironmentBuilder] Builder to add extracted information to.
        # @param event [Hash] Event from the Lambda handler.
        # @return [Hash] Rack environment.
        def self.env(builder, event)
          new(builder, event).build
        end

        # Creates an instance dedicated to populating a Rack environment
        # for a single event and context.
        # @param builder [EnvironmentBuilder] Builder to add extracted information to.
        # @param event [Hash] Event from the Lambda handler.
        def initialize(builder, event)
          @builder = builder
          @event   = event
        end

        # Populates the Rack environment builder with information from the API gateway event payload.
        # @return [void]
        def extract
          extract_common
          extract_headers
          extract_body
        end

        private

        # Rack environment builder instance.
        # @return [EnvironmentBuilder]
        attr_reader :builder

        # Event from the Lambda handler.
        # @return [Hash]
        attr_reader :event

        # Retrieves the HTTP headers from the Lambda event.
        # @return [Hash] HTTP headers.
        def headers
          event['headers'] || {}
        end

        # Retrieves information about the request.
        # @return [Hash] Request context.
        def request
          event.fetch('requestContext')
        end

        # Retrieves the HTTP specific information from the lambda event.
        # @return [Hash] HTTP information.
        def http
          request.fetch('http')
        end

        # Retrieves the HTTP request method from the Lambda event.
        # @return [String] HTTP request method.
        def request_method
          http.fetch('method')
        end

        # Retrieves the path information, or a default value.
        # @return [String]
        def path_info
          http['path'] || '/'
        end

        # Retrieves the query parameters from the Lambda event.
        # @return [Hash]
        def query_params
          event['queryStringParameters'] || {}
        end

        # Retrieves the server hostname, or a default value.
        # @return [String]
        def server_name
          request['domainName'] || 'localhost'
        end

        # Retrieves the server port, or a default value.
        # @return [Integer]
        def server_port
          headers.fetch('X-Forwarded-Port', 80).to_i
        end

        # Retrieves the URL scheme, or a default value.
        # @return [String]
        def url_scheme
          headers.fetch('CloudFront-Forwarded-Proto') do
            headers.fetch('X-Forwarded-Proto', 'http')
          end
        end

        # Configures common Rack environment attributes.
        # @return [void]
        def extract_common # rubocop:disable Metrics/AbcSize
          builder.request_method(request_method)
          builder.path_info(path_info)
          builder.query_params(query_params)
          builder.server_name(server_name)
          builder.server_port(server_port)
          builder.url_scheme(url_scheme)
        end

        # Configures the HTTP request headers.
        # @return [void]
        def extract_headers
          headers.each do |key, value|
            builder.header(key, value)
          end
        end

        # Configures the client request body portion.
        # @return [void]
        def extract_body
          return unless (body = event['body'])

          if event['isBase64Encoded']
            builder.base64_body(body)
          else
            builder.body(body)
          end
        end
      end
    end
  end
end

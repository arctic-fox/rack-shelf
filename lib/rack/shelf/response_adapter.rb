# frozen_string_literal: true

module Rack
  module Shelf
    # Transforms a standard Rack response array
    # to a return value required by AWS Lambda.
    # @see https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html#api-gateway-simple-proxy-for-lambda-output-format
    class ResponseAdapter
      # Converts a Rack response to one supported by AWS Lambda.
      # @param response [Array] Three-element array.
      #   This is the standard response from a Rack application.
      #   Must have the elements: status code, headers, and body.
      # @return [Hash] AWS Lambda response.
      # @see https://www.rubydoc.info/github/rack/rack/file/SPEC#label-The+Response
      def self.convert(response)
        new(*response).build
      end

      # Generates a Lambda response for an error.
      # @param exception [Exception, #to_s] Caught exception.
      # @param status_code [Integer] HTTP response code.
      # @return [Hash] AWS Lambda response.
      def self.error(exception, status_code = 500)
        new(status_code, {}, exception.to_s).build
      end

      # Creates an adapter dedicated to processing one response.
      # @param status_code [#to_i] HTTP status code.
      # @param headers [#each] HTTP headers.
      # @param body [#each] Response body.
      def initialize(status_code, headers, body)
        @status_code = status_code
        @headers     = headers
        @body        = body
      end

      # Constructs the AWS Lambda response.
      # @return [Hash]
      def build
        {
          'status_code' => status_code,
          'headers' => headers,
          'body' => body,
          'isBase64Encoded' => false
        }
      end

      private

      # The integer HTTP status code.
      # @return [Integer]
      def status_code
        @status_code.to_i
      end

      # Constructs the HTTP headers.
      # @return [Hash]
      def headers
        # Typically, the headers are already a hash.
        # But, the Rack Spec only requires the object to expose `#each`.
        {}.tap do |hash|
          @headers.each do |key, value|
            hash[key] = value
          end
        end
      end

      # Constructs the response body.
      # @return [String]
      def body
        StringIO.new.tap do |io|
          @body.each do |part|
            io.write(part)
          end
          @body.close if @body.respond_to?(:close)
        end.string
      end
    end
  end
end

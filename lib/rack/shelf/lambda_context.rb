# frozen_string_literal: true

module Rack
  module Shelf
    # Mix-in for populating a Rack environment with Lambda context information.
    module LambdaContext
      # Prefix used for all environment keys.
      PREFIX = 'aws.lambda'

      # Adds the AWS Lambda context information to the Rack environment.
      # @return [void]
      def build_lambda_context # rubocop:disable Metrics/AbcSize, Metrics/MethodLength
        @builder.application(PREFIX, 'function_name', @context.function_name)
        @builder.application(PREFIX, 'function_version', @context.function_version)
        @builder.application(PREFIX, 'invoked_function_arn', @context.invoked_function_arn)
        @builder.application(PREFIX, 'memory_limit_in_mb', @context.memory_limit_in_mb)
        @builder.application(PREFIX, 'aws_request_id', @context.aws_request_id)
        @builder.application(PREFIX, 'log_group_name', @context.log_group_name)
        @builder.application(PREFIX, 'log_stream_name', @context.log_stream_name)
        @builder.application(PREFIX, 'deadline_ms', @context.deadline_ms)
        @builder.application(PREFIX, 'identity', @context.identity)
        @builder.application(PREFIX, 'client_context', @context.client_context)
        @builder.application(PREFIX, 'remaining_time_in_millis', lambda {
          @context.get_remaining_time_in_millis
        })
      end
    end
  end
end

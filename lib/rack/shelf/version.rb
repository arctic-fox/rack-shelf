# frozen_string_literal: true

module Rack
  module Shelf
    VERSION = '0.0.3'
  end
end

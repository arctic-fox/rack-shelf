# frozen_string_literal: true

require 'base64'
require 'rack'
require 'stringio'

module Rack
  module Shelf
    # Builds up a Rack env hash.
    class EnvironmentBuilder
      # Default values for the Rack environment.
      DEFAULTS = {
        'REQUEST_METHOD' => 'GET',
        'SCRIPT_NAME' => '',
        'PATH_INFO' => '/',
        'QUERY_STRING' => '',
        'SERVER_NAME' => 'localhost',
        'SERVER_PORT' => '80',
        'rack.version' => Rack::VERSION,
        'rack.url_scheme' => 'http',
        'rack.input' => StringIO.new,
        'rack.errors' => $stderr,
        'rack.multithread' => false,
        'rack.multiprocess' => false,
        'rack.run_once' => false,
        'rack.hijack?' => false
      }.freeze

      # Creates the builder with reasonable defaults.
      def initialize
        @env = DEFAULTS.clone(freeze: false)
      end

      # Specifies the request method.
      # @param method [String] Must be one of:
      #   +GET+, +POST+, +PUT+, +HEAD+, +DELETE+, +PATCH+, or +OPTIONS+.
      # @return [void]
      def request_method(method)
        @env['REQUEST_METHOD'] = method
      end

      # Specifies the name (or mounting point) of the application.
      # @param name [String] Script name.
      # @return [void]
      def script_name(name)
        @env['SCRIPT_NAME'] = name
      end

      # Specifies the path info (path after the mounting point).
      # @param path [String] Path info.
      # @return [void]
      def path_info(path)
        @env['PATH_INFO'] = path
      end

      # Specifies the entire query string.
      # @param string [String] Pre-encoded query string.
      # @return [void]
      def query_string(string)
        @env['QUERY_STRING'] = string
      end

      # Specifies the query parameters as a hash.
      # @param params [Hash] Query parameters.
      # @return [void]
      def query_params(params)
        string = Rack::Utils.build_query(params)
        query_string(string)
      end

      # Specifies the hostname of the server.
      # @param name [String] Server name.
      # @return [void]
      def server_name(name)
        @env['SERVER_NAME'] = name
      end

      # Specifies the port the server is listening on.
      # @param port [#to_s] Port number.
      # @return [void]
      def server_port(port)
        @env['SERVER_PORT'] = port.to_s
      end

      # Specifies the URL scheme for the request.
      # @param scheme [String] Must be: +http+ or +https+.
      # @return [void]
      def url_scheme(scheme)
        @env['rack.url_scheme'] = scheme
      end

      # Specifies the stream used for input (request body).
      # @param io [IO] Input stream.
      # @return [void]
      def input_stream(io)
        @env['rack.input'] = io
      end

      # Specifies the client request body.
      # @param content [String] Request body.
      # @return [void]
      def body(content)
        io = StringIO.new(content)
        input_stream(io)
      end

      # Specifies the client request body encoded in base-64.
      # @param content [String] Base-64 encoded request body.
      # @return [void]
      def base64_body(content)
        decoded = Base64.decode64(content)
        body(decoded)
      end

      # Specifies the stream used to display errors.
      # @param io [IO] Error stream.
      # @return [void]
      def error_stream(io)
        @env['rack.errors'] = io
      end

      # Defines an HTTP header in the request.
      # @param header [String] HTTP header name.
      # @param value [String] Value of the HTTP header.
      # @return [void]
      def header(header, value)
        name = header.upcase.gsub('-', '_')
        key  = case name
               when 'CONTENT_TYPE', 'CONTENT_LENGTH'
                 name
               else
                 'HTTP_' + name
               end

        @env[key] = value
      end

      # Defines a custom application value.
      # @param prefix [String] Prefix of the key.
      # @param name [String] Name of the application key.
      # @param value [Object] Value of the key.
      # @return [void]
      # @raise [ArgumentError] The prefix can't be +rack+.
      def application(prefix, name, value)
        raise ArgumentError, "Prefix can't be 'rack'" if prefix == 'rack'

        key = [prefix, name].join('.')
        @env[key] = value
      end

      # Creates the Rack env hash.
      # @return [Hash]
      def build
        @env.clone
      end
    end
  end
end

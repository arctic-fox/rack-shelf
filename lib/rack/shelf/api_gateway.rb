# frozen_string_literal: true

require_relative 'api_gateway/v1_payload'
require_relative 'api_gateway/v2_payload'
require_relative 'environment_builder'
require_relative 'lambda_context'

module Rack
  module Shelf
    # Converts AWS API Gateway events given to Lambda
    # to Rack environment instances.
    class APIGateway
      include LambdaContext

      # Produces a Rack compatible environment instance
      # from a Lambda event and context.
      # @param event [Hash] Event from the Lambda handler.
      # @param context [Object] Context from the Lambda handler.
      # @return [Hash] Rack environment.
      def self.env(event, context)
        new(event, context).build
      end

      # Creates an instance dedicated to building a Rack environment
      # for a single event and context.
      # @param event [Hash] Event from the Lambda handler.
      # @param context [Object] Context from the Lambda handler.
      def initialize(event, context)
        @event   = event
        @context = context
        @builder = EnvironmentBuilder.new

        @implementation = case (version = event['version'])
                          when '1.0' then V1Payload.new(@builder, @event)
                          when '2.0' then V2Payload.new(@builder, @event)
                          else raise "Unrecognized API Gateway payload version #{version}"
                          end
      end

      # Builds the rack environment.
      # @return [Hash] Rack environment.
      def build
        @implementation.extract
        # build_lambda_context
        builder.build
      end

      private

      # Rack environment builder instance.
      # @return [EnvironmentBuilder]
      attr_reader :builder

      # Event from the Lambda handler.
      # @return [Hash]
      attr_reader :event

      # Context from the Lambda handler.
      # @return [Object]
      attr_reader :context
    end
  end
end
